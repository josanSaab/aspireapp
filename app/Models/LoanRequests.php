<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanRequests extends Model
{
    use HasFactory;

    protected $table = 'loan_requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'loan_amount',
        'loan_term',
        'is_approved',
        'requested_on',
        'interest',
    ];
}
