<?php

/**
 * 
 * @function emiCalculator
 * @param $term,$amount,$interest
 * @response Array
 */

if(!function_exists('emiCalculator')){
    function emiCalculator($term,$amount,$interest)
    {
        $rate = ($interest / 12) / 1000; // weekly interest rate
        $payment = ($rate * $amount) * round(pow((1 + $rate), $term), 5) / (round(pow((1 + $rate), $term), 5) - 1);
        $allEmis = [];
        for ($i = 1; $i <= $term; $i++) {
            $allEmis[] = $payment;
            
        }
        return ['all_emis' => $allEmis,'single_emi' => $payment];
    }
}