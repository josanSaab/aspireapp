<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
use Carbon\Carbon;
use App\Models\LoanRequests;
use LoanRequest;
use App\Helpers;
use App\Models\LoanRepayTransactions;
use Illuminate\Support\Facades\DB;

class LoanController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Processing for loan request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loanRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_amount' => 'required',
            'loan_term' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
       $loanAmount = $request->loan_amount;
       $loanTerm = $request->loan_term;
       $requestedOn = Carbon::now()->toDateTimeString();
       $totalEmis = $request->loan_emis;
       $interest = $request->interest;
       try {
            $loanDetails = [
                'user_id' => auth()->user()->id,
                'loan_amount' => $loanAmount,
                'loan_term' => $loanTerm,
                'requested_on' => $requestedOn,
                'interest' => 10, // 10 percent hardcoded
            ];
            LoanRequests::create($loanDetails);
            return response()->json([
                'status' => 'success',
                'message' => 'Loan Applied Successfully',
                'user' => auth()->user(),
            ], 201);
       } catch (\Throwable $th) {
           throw $th; // Error Logging
       }

    }

    /**
     * Process for loan approval
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function loanApproval(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_request_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $where = ['id'=>$request->loan_request_id];
        $loanDetails = LoanRequests::where($where)->first();
        $term = $loanDetails->loan_term;
        $amount = $loanDetails->loan_amount;
        $interest = $loanDetails->interest;
        $emis = emiCalculator($term, $amount, $interest); //helper function for calculating emis
        $date = date('Y-m-d H:i:s');
         DB::beginTransaction(); // Transaction begin
        foreach ($emis['all_emis'] as $key => $emi) {
            $insert = [
                'loan_request_id' => $loanDetails->id,
                'emi' => $emi,
                'user_id' => $loanDetails->user_id,
                'principal' => $amount,
                'interest' => $loanDetails->interest,
                'is_paid' => 0, //Not paid
                'created_at' => $date,
                'updated_at' => $date,
            ];
            LoanRepayTransactions::create($insert);
            LoanRequests::where($where)->update(['is_approved'=>'1']); // Loan approval success
            DB::commit(); //Transaction commit
        }
            return response()->json([
                'status' => 'success',
                'message' => 'Loan Approved Successfully',
            ], 201);
    }

    /**
     * Weekly Emi payment process
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function weeklyRepayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_request_id' => 'required',
            'emi' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $userId = auth()->user()->id;
        $where = ['user_id' => $userId, 'is_paid'=> 0, 'loan_request_id' => $request->loan_request_id];
        $loanDetails = LoanRepayTransactions::where($where)->first();
       if(!empty($loanDetails)){
            if($loanDetails->emi == $request->emi){ //if the emi amount is same. It should be same from the frontend
                LoanRepayTransactions::where($where)->where('id', $loanDetails->id)->update(['is_paid'=>1]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Emi Paid Successfully',
                    'user' => auth()->user(),
                ], 201);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong',
                ], 402);
            }
    }else{
            LoanRequests::where('user_id',$userId)->where('id', $request->loan_request_id)->update(['is_settled'=>1]);
            return response()->json([
                'status' => 'success',
                'message' => 'Emis fully paid',
                'user' => auth()->user(),
            ], 201);
    }
    }
    
}
