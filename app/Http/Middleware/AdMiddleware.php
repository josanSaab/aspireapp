<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // check if approver is admin | else unauthorized user
        if (auth()->user()->role_id == 1) {
            return $next($request);
        }
        else{
            return response()->json(
                ['error' => 'Unauthorized','message' => "You are not authorize to perform this action"]
                , 401);
        }
        return $next($request);
    }
}
