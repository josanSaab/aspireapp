<p align="center"><a href="https://aspireapp.com/" target="_blank"><img src="https://global-uploads.webflow.com/5ed5b60be1889f546024ada0/5ed8a32c8e1f40c8d24bc32b_Aspire%20Logo%402x.png" width="400"></a></p>

## About AspireApp

AspireMini is a weekly payment Set of API's by which a user can request the loan amount and admin can verify it. Once the approval is done, the user can pay the emi's on the weekly basis. 

## Integrating AspireApp

Clone the application at your local by simply using this command <b>git clone https://josanSaab@bitbucket.org/josanSaab/aspireapp.git</b>. After cloning is done setup your .env file as per you choice. The .env file is already at it's place as you can use the same configurations.(Please update the port and unix_socket in .env file)

After the setup is done simply hit the command <b>php artisan migrate</b> to populate your database for the required details on your local setup. Once the migration is done you will find a <b>admin entry into the Users table with role_id = 1.

Admin User: admin@admin.com
Password: 123456
</b>

Now import the postman collection to your Postman. There are 5 API's in the collection(Register, Login, Loan Request, Loan Approval, Weekly Repayment).

Register yourself with the required details. Once you get the success message for registration then move the next step for login.Once the login is done you will get bearer access_token into the response. Now in the authoriaztion tab simply select the Bearer Token for accessing the further API's, Otherwise you will face an unauthorized message in your response tab. (Please try to set a global variable)

<b>Note: The token in the response is JSON Web Token (JWT) which is open standard used to share security information.</b> 

After adding the token user can access the further API's. 

## API Explanations

<b>Loan Request: </b>
Endpoint: http://127.0.0.1:8000/api/loan/request?loan_amount=100&loan_term=4
Table: loan_requests
Description: A user can request for a loan amount by simply providing the loan term. These two fields are required in the payload else user will get the validation error. Once these two will be provided the user will get the success message with user details.

<b>Loan Approval: </b>
Endpoint: http://127.0.0.1:8000/api/loan/approve?loan_request_id=1
Table: loan_repay_transactions
Description: Once user has requested for the loan now admin will get the notification on the dashboard or CRM that one loan request has come. After reviewing the loan amount and the term admin can sanction the loan. (Other user which is not admin can not approve the loan as there is a filter or validation in place.) Once the loan is approved now the table 

<b>Weekly Repayments: </b>
Endpoint: http://127.0.0.1:8000/api/loan/repay?emi=25.03&loan_request_id=1
Table: loan_repay_transactions
Description: Now the End user can pay weekly EMI's by using it. Emi and loan_request_id will be received from the fronted side and both are required. if the user had selected for the term 4 which is weekly, can pay the emi one by one for 4 times on weekly basis. When one emi will be paid End user will get the success message for the Emi paid. Once all the EMI's will be paid then the user will get the message that all the EMI's are paid and the account is settled.


<b>Note: This was the easiest and quickest way possible due to lack of time. There could be many improvement like: user will get the weekly notifications. Admin could get the notification regarding the payment delay for a particular user and so on.

<b>HAPPY CODING</b>