<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LoanRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->float('loan_amount',8,2)->unsigned();
            $table->float('loan_term',8,2)->unsigned();
            $table->enum('is_approved', ['0', '1'])->nullable()->default('0')->comment('0 => Not approved, 1=> Approved');;
            $table->dateTime('requested_on');
            // $table->integer('total_emis')->unsigned();
            $table->integer('interest')->unsigned()->comment('hardcoded');
            $table->timestamps();
        });

        Schema::create('loan_repay_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('loan_request_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('emi',8,2)->unsigned();
            $table->float('principal',8,2)->unsigned();
            $table->float('interest',8,2)->unsigned()->comment('Annual Interest');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
        Schema::dropIfExists('loan_repay_details');
    }
}
