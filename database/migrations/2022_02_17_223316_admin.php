<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pass = bcrypt('123456');
        $date = date('Y-m-d H:i:s');
        DB::statement("INSERT INTO aspireapp.users (name,email,role_id,email_verified_at,password,remember_token,created_at,updated_at) VALUES
        ('Admin','admin@admin.com',1,NULL,'$pass',NULL,'$date','$date')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
