<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Allemis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_repay_transactions', function (Blueprint $table) {
            $table->integer('is_paid')->after('interest')->nullable();
        });

        Schema::table('loan_requests', function (Blueprint $table) {
            $table->integer('is_settled')->after('interest')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_repay_transactions', function (Blueprint $table) {
            $table->dropColumn('is_paid');
        });

        Schema::table('loan_requests', function (Blueprint $table) {
            $table->dropColumn('is_settled');
        });
    }
}
